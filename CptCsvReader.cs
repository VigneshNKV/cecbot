﻿using CECBOT.Extenders;
using System;
using System.Collections.Generic;
using System.IO;

namespace CECBOT
{
    public class CptCsvReader : BaseCsvReader
    {
        private readonly Dictionary<ECptColumns, int> cptColIndexes;
        public CptCsvReader(string path) : base(path)
        {
            cptColIndexes = new Dictionary<ECptColumns, int>()
            {
                { ECptColumns.Code, -1},
                { ECptColumns.Descrpition, -1},
                { ECptColumns.Ndc, -1},
            };
        }

        public IEnumerable<CPTMasterData> Read()
        {
            var cptMasterDataList = new List<CPTMasterData>();
            bool skipNextline = false;

            var lines = File.ReadAllLines(_path);

            var headerLine = lines[0];
            var headerArray = headerLine.Split(',');

            GetIndexOfHeaderColumns(headerArray);

            for (int i = 1; i < lines.Length; i++)
            {
                if (skipNextline)
                {
                    skipNextline = false;
                    continue;
                }

                try
                {
                    var line = lines[i];

                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    var lineItems = line.Split(',');

                    lineItems = HandleLinesThatWrapToNextLine(lines, headerArray, i, lineItems, ref skipNextline);

                    CPTMasterData cptMasterData = new CPTMasterData() { };

                    try
                    {
                        if (cptColIndexes[ECptColumns.Code] >= 0 && lineItems.Length >= (cptColIndexes[ECptColumns.Code] - 1))
                            cptMasterData.Code = lineItems[cptColIndexes[ECptColumns.Code]].RemoveDoubleQuotes();

                        if (cptColIndexes[ECptColumns.Descrpition] >= 0 && lineItems.Length >= (cptColIndexes[ECptColumns.Descrpition] - 1))
                            cptMasterData.Description = lineItems[cptColIndexes[ECptColumns.Descrpition]].RemoveDoubleQuotes();

                        if (cptColIndexes[ECptColumns.Ndc] >= 0 && lineItems.Length >= (cptColIndexes[ECptColumns.Ndc] - 1))
                            cptMasterData.NdcCode = lineItems[cptColIndexes[ECptColumns.Ndc]].RemoveDoubleQuotes();

                        cptMasterDataList.Add(cptMasterData);
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return cptMasterDataList;
        }

        /// <summary>
        /// Handles lines that are broken into two lines, as shown below
        /// 45000,"Drain Pelvic Abscess
        /// ",6271,Procedure charge,,
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="headerArray"></param>
        /// <param name="i"></param>
        /// <param name="lineItems"></param>
        /// <param name="skipNextline"></param>
        /// <returns></returns>
        private static string[] HandleLinesThatWrapToNextLine(string[] lines, string[] headerArray, int i, string[] lineItems, ref bool skipNextline)
        {
            skipNextline = false;

            if (i < lines.Length - 1)
            {
                var combinedLines = lines[i].RemoveDoubleQuotes() + lines[i + 1].RemoveDoubleQuotes();

                var combinedLineItems = combinedLines.Split(',');

                if (combinedLineItems.Length == headerArray.Length)
                {
                    lineItems = combinedLineItems;
                    skipNextline = true;
                }
            }

            return lineItems;
        }

        private void GetIndexOfHeaderColumns(string[] headerArray)
        {
            var settings = SettingsManager.SettingsCache.CptFileSettings;

            for (int i = 0; i < headerArray.Length; i++)
            {
                AddToCptColIndexes(headerArray, settings.Code, ECptColumns.Code, i);
                AddToCptColIndexes(headerArray, settings.Description, ECptColumns.Descrpition, i);
                AddToCptColIndexes(headerArray, settings.Ndc, ECptColumns.Ndc, i);
            }
        }

        private void AddToCptColIndexes(string[] headerArray, string setting, ECptColumns cptColumns, int colIndex)
        {
            headerArray[colIndex] = headerArray[colIndex].RemoveDoubleQuotes();
            if (headerArray[colIndex].ToLower() == setting.ToLower())
            {
                cptColIndexes[cptColumns] = colIndex;
            }
        }
    }
}
