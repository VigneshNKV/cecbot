﻿using CECBOT.Specifications;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CECBOT
{
    public class ExcelFileProcessor
    {
        private readonly IEnumerable<ICDMasterData> _icdMasterData;
        private readonly IEnumerable<CPTMasterData> _cptMasterData;
        private IEnumerable<string> LastReadPrimaryIcdValues { get; set; }
        private bool IgnoreCpt => _cptMasterData == null || !_cptMasterData.Any();

        private bool IgnoreIcd => _icdMasterData == null || !_icdMasterData.Any();

        public ExcelFileProcessor(IEnumerable<ICDMasterData> iCDMasterData, IEnumerable<CPTMasterData> cBCMasterData)
        {
            _icdMasterData = iCDMasterData;
            _cptMasterData = cBCMasterData;
        }

        public ExcelFileProcessor()
        {

        }
        public static IEnumerable<string> GetHeaders(string path)
        {
            var fi = new FileInfo(path);
            using (var package = new ExcelPackage(fi))
            {
                var workbook = package.Workbook;
                var worksheet = workbook.Worksheets.First();
                var start = worksheet.Dimension.Start;
                var end = worksheet.Dimension.End;

                for (int col = start.Column; col <= end.Column; col++)
                {
                    yield return worksheet.Cells[1, col].Text.Trim();
                }
            }
        }

        public async Task ProcessAsync(string path)
        {
            await Task.Run(() =>
           {
               var fi = new FileInfo(path);

               using (var package = new ExcelPackage(fi))
               {
                   var workbook = package.Workbook;
                   var worksheet = workbook.Worksheets.First();
                   var start = worksheet.Dimension.Start;
                   var end = worksheet.Dimension.End;
                   var cptColIndex = 0;
                   var icdColIndex = 0;
                   var mrnColIndex = 0;
                   var statusColIndex = 0;
                   var InputfileBillingCommentsColIndex = 0;
                   var inputFileNdcColIndex = 0;

                   CheckAndGetColumnIndexes(worksheet,
                       start,
                       end,
                       ref cptColIndex,
                       ref inputFileNdcColIndex,
                       ref InputfileBillingCommentsColIndex,
                       ref icdColIndex,
                       ref mrnColIndex,
                       ref statusColIndex);

                   ColorConverter colorConverter = new ColorConverter();
                   Color color = (Color)colorConverter.ConvertFromString(SettingsManager.SettingsCache.InputFileSettings.HighlightColor);

                   for (int row = start.Row + 1; row <= end.Row; row++)
                   {
                       if (!IgnoreCpt)
                       {
                           string cptValue = worksheet.Cells[row, cptColIndex].Text.Trim();
                           cptValue = ValidateCPT(color, worksheet, row, cptValue, cptColIndex, InputfileBillingCommentsColIndex, inputFileNdcColIndex);
                       }

                       if (!IgnoreIcd)
                       {
                           string mrn = worksheet.Cells[row, mrnColIndex].Text;
                           var icdValue = worksheet.Cells[row, icdColIndex].Text.Trim();
                           ValidateICD(worksheet, row, mrn, icdValue, statusColIndex, icdColIndex, color);
                       }
                   }

                   package.Save();
               }
           });
        }

        private void CheckAndGetColumnIndexes(
            ExcelWorksheet worksheet,
            ExcelCellAddress start,
            ExcelCellAddress end,
            ref int cptColIndex,
            ref int ndcColIndex,
            ref int billingCommentsIndex,
            ref int icdColIndex,
            ref int mrnColIndex,
            ref int statusColIndex)
        {
            var settings = SettingsManager.SettingsCache;

            for (int col = start.Column; col <= end.Column; col++)
            {
                var cellValue = worksheet.Cells[1, col].Text;

                if (!IgnoreCpt)
                {
                    if (cellValue.Trim().ToLower() == settings.InputFileSettings.CptColumnName.ToLower())
                    {
                        cptColIndex = col;
                    }

                    if (cellValue.Trim().ToLower() == settings.InputFileSettings.NdcColumnName.ToLower())
                    {
                        ndcColIndex = col;
                    }

                    if (cellValue.Trim().ToLower() == settings.InputFileSettings.BillingCommentsColumnName.ToLower())
                    {
                        billingCommentsIndex = col;
                    }
                }

                if (!IgnoreIcd)
                {
                    if (cellValue.Trim().ToLower() == settings.InputFileSettings.IcdColumnName.ToLower())
                    {
                        icdColIndex = col;
                    }

                    if (cellValue.Trim().ToLower() == settings.InputFileSettings.MrnColumnName.ToLower())
                    {
                        mrnColIndex = col;
                    }

                    if (cellValue.Trim().ToLower() == settings.InputFileSettings.StatusColumnName.ToLower())
                    {
                        statusColIndex = col;
                    }
                }
            }
        }

        private void ValidateICD(ExcelWorksheet worksheet, int row, string mrn, string icdValue, int statusColIndex, int icdColIndex, Color highlightColor)
        {
            if (!string.IsNullOrWhiteSpace(mrn))
            {
                StringBuilder sb = new StringBuilder();

                if (!string.IsNullOrWhiteSpace(icdValue))
                {
                    string[] icdValues = icdValue.Split(',').Select(x => x.Trim()).ToArray();

                    HighlightDuplicateIcdValues(worksheet, row, icdColIndex, highlightColor, icdValues);

                    LastReadPrimaryIcdValues = icdValues;

                    foreach (var item in icdValues)
                    {
                        if (!_icdMasterData.Any(i => i.Code == item.Trim()))
                        {
                            sb.Append($"{item.Trim()}, ");
                        }
                    }

                    if (sb.Length > 0)
                    {
                        sb.Length--;
                        sb.Length--;

                        worksheet.Cells[row, statusColIndex].Value = $"{sb.ToString()} ICD code(s) not found";
                    }
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(icdValue))
                {
                    string[] icdValues = icdValue.Split(',').Select(x => x.Trim()).ToArray();
                    var validicdValues = icdValues.Intersect(LastReadPrimaryIcdValues);

                    if (validicdValues == null || !validicdValues.Any())
                    {
                        FormatIcdCell(worksheet, row, icdColIndex, highlightColor);
                    }
                }
            }
        }

        private static void HighlightDuplicateIcdValues(ExcelWorksheet worksheet, int row, int icdColIndex, Color highlightColor, string[] icdValues)
        {
            var groupedValues = icdValues.GroupBy(g => g.Trim()).Select(grp => grp);
            var duplicates = groupedValues.Any(grp => grp.Count() > 1);

            if (duplicates)
            {
                FormatIcdCell(worksheet, row, icdColIndex, highlightColor);
            }
        }

        private static void FormatIcdCell(ExcelWorksheet worksheet, int row, int icdColIndex, Color highlightColor)
        {
            worksheet.Cells[row, icdColIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[row, icdColIndex].Style.Fill.BackgroundColor.SetColor(highlightColor);
        }

        private string ValidateCPT(Color color,
            ExcelWorksheet worksheet,
            int row,
            string cptValue,
            int cptColIndex,
            int billingCommentsIndex,
            int ndcIndex)
        {
            if (!string.IsNullOrWhiteSpace(cptValue))
            {
                if (cptValue.Contains("*"))
                {
                    cptValue = cptValue.Split('*')[0].Trim();
                }
                else
                {
                    cptValue = cptValue.Trim();
                }

                var cptData = _cptMasterData.FirstOrDefault(c => c.Code == cptValue);
                var cptCodeMissingSpecification = new CptCodeMissingSpecification(_cptMasterData);
                var cptNdcSpecification = new CptNdcSpecification(cptData?.NdcCode);
                var SurgicalSupplySpecification = new SurgicalSupplySpecification();

                if (cptCodeMissingSpecification.IsSatisfiedBy(cptValue))
                {
                    worksheet.Cells[row, cptColIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, cptColIndex].Style.Fill.BackgroundColor.SetColor(color);
                }
                else if (cptNdcSpecification.IsSatisfiedBy(cptValue))
                {
                    var billingComments =
                        worksheet.Cells[row, billingCommentsIndex].Value == null
                        ? null
                        : string.IsNullOrWhiteSpace(worksheet.Cells[row, billingCommentsIndex].Value.ToString())
                        ? null
                        : worksheet.Cells[row, billingCommentsIndex].Value;

                    var ndcCode =
                        worksheet.Cells[row, ndcIndex].Value == null
                        ? null
                        : string.IsNullOrWhiteSpace(worksheet.Cells[row, ndcIndex].Value.ToString())
                        ? null
                        : worksheet.Cells[row, ndcIndex].Value;

                    if ((billingComments != null && ndcCode != null)
                        || SurgicalSupplySpecification.IsSatisfiedBy(billingComments?.ToString()))
                    {
                        return cptValue;
                    }

                    if (billingComments == null)
                    {
                        worksheet.Cells[row, billingCommentsIndex].Value = cptData.Description;
                    }
                    if (ndcCode == null)
                    {
                        worksheet.Cells[row, ndcIndex].Value = cptData.NdcCode;
                    }

                    worksheet.Cells[row, ndcIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, ndcIndex].Style.Fill.BackgroundColor.SetColor(color);
                    worksheet.Cells[row, billingCommentsIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[row, billingCommentsIndex].Style.Fill.BackgroundColor.SetColor(color);
                }
            }

            return cptValue;
        }
    }
}
