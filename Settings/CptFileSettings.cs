﻿using System;
using System.Collections.Generic;

namespace CECBOT
{
    public class CptFileSettings
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Ndc { get; set; }
        public string SampleFileName { get; set; }

        public List<CPTRange> CPTRange { get; set; }
        public List<string> Series { get; set; }

        public CptFileSettings()
        {
            Series = new List<string>();
            CPTRange = new List<CPTRange>();
        }
    }

    public class CPTRange
    {
        public string StartValue { get; set; }
        public string EndValue { get; set; }

        public long StartValueNumber
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(StartValue))
                {
                    return Convert.ToInt64(StartValue);
                }

                return 0;
            }
        }


        public long EndValueNumber
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(EndValue))
                {
                    return Convert.ToInt64(EndValue);
                }

                return 0;
            }
        }

        public string Prefix { get; set; }

        public string Range
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Prefix))
                {
                    return $"{StartValue}-{EndValue}";
                }
                else
                {
                    return $"{Prefix}{StartValue}-{Prefix}{EndValue}";
                }
            }
        }

        public CPTRange()
        {

        }
        public CPTRange(string item)
        {
            var range = item.Split('-');
            var start = range[0].Trim();
            var end = range[1].Trim();

            if (char.IsLetter(start[0]))
            {
                Prefix = start[0].ToString();
                StartValue = start.Substring(1, start.Length - 1);
            }
            else
            {
                StartValue = start;
            }

            if (char.IsLetter(end[0]))
            {
                EndValue = end.Substring(1, end.Length - 1);
            }
            else
            {
                EndValue = end;
            }
        }
    }
}
