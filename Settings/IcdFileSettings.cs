﻿namespace CECBOT
{
    public class IcdFileSettings
    {
        public string Code { get; set; }
        public string SampleFileName { get; set; }
    }
}
