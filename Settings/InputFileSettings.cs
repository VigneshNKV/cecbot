﻿namespace CECBOT
{
    public class InputFileSettings
    {
        public string HighlightColor { get; set; }
        public string CptColumnName { get; set; }
        public string MrnColumnName { get; set; }
        public string StatusColumnName { get; set; }
        public string IcdColumnName { get; set; }
        public string SampleFileName { get; set; }
        public string NdcColumnName { get; set; }
        public string BillingCommentsColumnName { get; set; }
    }
}
