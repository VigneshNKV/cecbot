﻿using System.Collections.Generic;
using System.IO;

namespace CECBOT
{
    public abstract class BaseCsvReader
    {
        protected string _path { get; set; }
        public BaseCsvReader(string path)
        {
            _path = path;
        }
        public virtual IEnumerable<string> GetHeaders()
        {
            var lines = File.ReadAllLines(_path);

            var headerLine = lines[0];
            var headerArray = headerLine.Split(',');

            foreach (var item in headerArray)
            {
                yield return item.Replace("\"", string.Empty);
            }
        }
    }
}
