﻿using CECBOT.Extenders;
using System.Collections.Generic;
using System.IO;

namespace CECBOT
{
    public class IcdCsvReader : BaseCsvReader
    {
        private Dictionary<EIcdColumns, int> IcdColIndexes;
        private IcdFileSettings _icdFileSettings = SettingsManager.SettingsCache.IcdFileSettings;

        public IcdCsvReader(string path) : base(path)
        {
            IcdColIndexes = new Dictionary<EIcdColumns, int>()
            {
                { EIcdColumns.Code, -1},
            };
        }

        public IEnumerable<ICDMasterData> Read()
        {
            List<ICDMasterData> cptMasterDataList = new List<ICDMasterData>();

            var lines = File.ReadAllLines(_path);

            var headerLine = lines[0];
            var headerArray = headerLine.Split(',');

            GetIndexOfHeaderColumns(headerArray);

            for (int i = 1; i < lines.Length; i++)
            {
                var line = lines[i];

                var lineItems = line.Split(',');

                ICDMasterData cptMasterData = new ICDMasterData() { };
                cptMasterData.Code = lineItems[IcdColIndexes[EIcdColumns.Code]].RemoveDoubleQuotes();
                cptMasterDataList.Add(cptMasterData);
            }

            return cptMasterDataList;
        }

        private void GetIndexOfHeaderColumns(string[] headerArray)
        {
            for (int i = 0; i < headerArray.Length; i++)
            {
                headerArray[i] = headerArray[i].RemoveDoubleQuotes();

                if (headerArray[i].ToLower() == _icdFileSettings.Code.ToLower())
                {
                    IcdColIndexes[EIcdColumns.Code] = i;
                }
            }
        }
    }
}
