﻿using CECBOT.Extenders;
using System;
using System.Linq;

namespace CECBOT.Specifications
{
    public class CptRangeSpecification : ISpecification<string>
    {
        private readonly CptFileSettings cptSettings = SettingsManager.SettingsCache.CptFileSettings;
        public bool IsSatisfiedBy(string cptCode)
        {

            if (string.IsNullOrWhiteSpace(cptCode))
            {
                return false;
            }

            foreach (var seriesValue in cptSettings.Series)
            {
                if (cptCode.StartsWith(seriesValue))
                {
                    return true;
                }
            }

            if (char.IsLetter(cptCode[0]))
            {
                var ranges = cptSettings.CPTRange.Where(r => !string.IsNullOrWhiteSpace(r.Prefix) && r.Prefix.ToLower() == cptCode[0].ToString().ToLower());

                foreach (var range in ranges)
                {
                    if (cptCode.IsOnlyAlphabets())
                    {
                        continue;
                    }

                    var cptNumericValue = Convert.ToInt64(cptCode.Substring(1, cptCode.Length - 1));

                    if (cptNumericValue >= range.StartValueNumber && cptNumericValue <= range.EndValueNumber)
                    {
                        return true;
                    }
                }
            }
            else
            {
                var ranges = cptSettings.CPTRange.Where(r => string.IsNullOrWhiteSpace(r.Prefix));

                foreach (var range in ranges)
                {
                    var cptNumericValue = Convert.ToInt64(cptCode);

                    if (cptNumericValue >= range.StartValueNumber && cptNumericValue <= range.EndValueNumber)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
