﻿namespace CECBOT
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T entity);
    }
}
