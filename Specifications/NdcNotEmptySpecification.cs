﻿namespace CECBOT
{
    public class NdcNotEmptySpecification : ISpecification<string>
    {
        public bool IsSatisfiedBy(string ndcCode)
        {
            return !string.IsNullOrWhiteSpace(ndcCode);
        }

    }
}
