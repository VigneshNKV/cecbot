﻿namespace CECBOT.Specifications
{
    public class SurgicalSupplySpecification : ISpecification<string>
    {
        public bool IsSatisfiedBy(string ndcDescription)
        {
            if (!string.IsNullOrWhiteSpace(ndcDescription))
            {
                if (ndcDescription.ToLower().Contains("surgical supply")
                    || ndcDescription.ToLower().Contains("surgical supplies"))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
