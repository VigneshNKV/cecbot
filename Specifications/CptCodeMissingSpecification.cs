﻿using System.Collections.Generic;
using System.Linq;

namespace CECBOT
{
    public class CptCodeMissingSpecification : ISpecification<string>
    {
        private readonly IEnumerable<CPTMasterData> _cptMasterData;
        public CptCodeMissingSpecification(IEnumerable<CPTMasterData> cptMasterData)
        {
            _cptMasterData = cptMasterData;
        }
        public bool IsSatisfiedBy(string cptValue)
        {
            var cptData = _cptMasterData.FirstOrDefault(c => c.Code == cptValue.Trim());
            return cptData == null;
        }
    }
}
