﻿namespace CECBOT.Specifications
{

    public class CptNdcSpecification : ISpecification<string>
    {
        private readonly string _ndcCode;

        public CptNdcSpecification(string ndcCode)
        {
            _ndcCode = ndcCode;
        }
        public bool IsSatisfiedBy(string cptCode)
        {
            var ndcNotEmptySpecification = new NdcNotEmptySpecification();
            var cptRangeSpecification = new CptRangeSpecification();
            return cptRangeSpecification.IsSatisfiedBy(cptCode);
                //&& ndcNotEmptySpecification.IsSatisfiedBy(_ndcCode);
        }
    }
}
