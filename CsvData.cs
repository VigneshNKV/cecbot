﻿using System;
using System.Collections.Generic;

namespace CECBOT
{
    public class CsvData<T>
    {
        public List<T> Rows { get; set; }
        public String ColumnNames { get; set; }
    }
}
