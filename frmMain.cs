﻿using FontAwesome.Sharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECBOT
{
    public partial class FrmMain : Form
    {
        private static readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(FrmMain));

        public FrmMain()
        {
            InitializeComponent();
        }

        private async void BtnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ToggleControls(isEnabled: false);

                HideStatus();

                ClearValidationErrors();

                ValidateControls();

                if (HasFormAnyError())
                {
                    return;
                }

                IEnumerable<CPTMasterData> cptMaster = null;
                IEnumerable<ICDMasterData> icdMaster = null;

                if (!string.IsNullOrWhiteSpace(txtCptPath.Text))
                {
                    CptCsvReader cptCsvReader = new CptCsvReader(txtCptPath.Text);

                    try
                    {
                        cptMaster = cptCsvReader.Read();
                    }
                    catch (IOException)
                    {
                        throw new IOException($"En error occcured trying to access the file {txtCptPath.Text}. Please close the file, it is open.");
                    }
                }

                if (!string.IsNullOrWhiteSpace(txtICDPath.Text))
                {
                    IcdCsvReader icdCsvReader = new IcdCsvReader(txtICDPath.Text);

                    try
                    {
                        icdMaster = icdCsvReader.Read();
                    }
                    catch (IOException)
                    {
                        throw new IOException($"En error occcured trying to access the file {txtICDPath.Text}. Please close the file, it is open.");
                    }

                }

                ExcelFileProcessor excelFileProcessor = new ExcelFileProcessor(icdMaster, cptMaster);
                await excelFileProcessor.ProcessAsync(txtInputPath.Text);

                await RememberInput();

                grpSuccess.Visible = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ToggleControls(isEnabled: true);
            }


        }

        private async Task RememberInput()
        {
            if (chkRemember.Checked)
            {
                SettingsManager.SettingsCache.LastCptPath = txtCptPath.Text;
                SettingsManager.SettingsCache.LastIcdPath = txtICDPath.Text;
                SettingsManager.SettingsCache.LastInputFilePath = txtInputPath.Text;
            }
            else
            {
                SettingsManager.SettingsCache.LastCptPath = "";
                SettingsManager.SettingsCache.LastIcdPath = "";
                SettingsManager.SettingsCache.LastInputFilePath = "";
            }

            await SettingsManager.UpdateSettings();
        }

        private void ToggleControls(bool isEnabled)
        {
            txtCptPath.Enabled = isEnabled;
            txtICDPath.Enabled = isEnabled;
            txtInputPath.Enabled = isEnabled;
            btnCPTFileDialog.Enabled = isEnabled;
            btnOpenInputFileDialog.Enabled = isEnabled;
            btnOpenICDFileDialog.Enabled = isEnabled;
            btnProcess.Enabled = isEnabled;
            btnClearForm.Enabled = isEnabled;
            btnClearForm.Enabled = isEnabled;
        }

        private void ValidateControls()
        {
            if (string.IsNullOrWhiteSpace(txtCptPath.Text) && string.IsNullOrWhiteSpace(txtICDPath.Text))
            {
                lblMasterDataPathError.Text = "Either CPT path or ICD path is required.";
                return;
            }

            ValidateCPTPath();

            ValidateICDPath();

            ValidateInputPath();
        }

        private void ClearValidationErrors()
        {
            lblCptError.Text = "";
            lblICDError.Text = "";
            lblInputError.Text = "";
            lblMasterDataPathError.Text = "";
        }

        private void ValidateInputPath()
        {
            if (string.IsNullOrWhiteSpace(txtInputPath.Text))
            {
                lblInputError.Text = "Input path is required";
            }
            else if (!File.Exists(txtInputPath.Text))
            {
                lblInputError.Text = "Input path is not valid";
            }
            else if (Path.GetExtension(txtInputPath.Text).ToLower() != ".xlsx")
            {
                lblInputError.Text = "Input file must be a XLSX file";
            }
        }

        private bool HasFormAnyError()
        {
            return
                !string.IsNullOrEmpty(lblCptError.Text)
                || !string.IsNullOrEmpty(lblICDError.Text)
                || !string.IsNullOrEmpty(lblInputError.Text)
                || !string.IsNullOrEmpty(lblMasterDataPathError.Text);
        }
        private void ValidateICDPath()
        {
            if (!string.IsNullOrWhiteSpace(txtICDPath.Text))
            {
                if (!File.Exists(txtICDPath.Text))
                {
                    lblICDError.Text = "ICD Master data file path is not valid";
                }
                else if (Path.GetExtension(txtICDPath.Text).ToLower() != ".csv")
                {
                    lblICDError.Text = "ICD Master data file must be a CSV file";
                }
            }
        }

        private void HideStatus()
        {
            grpSuccess.Visible = false;
        }

        private void ValidateCPTPath()
        {
            if (!string.IsNullOrWhiteSpace(txtCptPath.Text))
            {
                if (!File.Exists(txtCptPath.Text))
                {
                    lblCptError.Text = "CPT Master data file path is not valid";
                }
                else if (Path.GetExtension(txtCptPath.Text).ToLower() != ".csv")
                {
                    lblCptError.Text = "CPT Master data file must be a CSV file";
                }
            }
        }

        private void BtnCPTFileDialog_Click(object sender, EventArgs e)
        {
            CptFileDialog.Filter = "Comma Separated Files|*.csv;";

            if (CptFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtCptPath.Text = CptFileDialog.FileName;
                lblCptError.Text = "";
                lblMasterDataPathError.Text = "";
            }
        }

        private void BtnOpenInputFileDialog_Click(object sender, EventArgs e)
        {
            InpuFileDialog.Filter = "Excel Files|*.xlsx;";

            if (InpuFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtInputPath.Text = InpuFileDialog.FileName;
                lblInputError.Text = "";
            }
        }

        private void BtnOpenICDFileDialog_Click(object sender, EventArgs e)
        {
            IcdFileDialog.Filter = "Comma Separated Files|*.csv;";

            if (IcdFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtICDPath.Text = IcdFileDialog.FileName;
                lblICDError.Text = "";
                lblMasterDataPathError.Text = "";
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            _logger.Info("Main form Loading");

            await SettingsManager.LoadSettings();

            pictureBox1.Image = Image.FromFile("Resources/calpion_logo2.png");

            ClearValidationErrors();

            HideStatus();

            txtCptPath.Text = SettingsManager.SettingsCache.LastCptPath;
            txtICDPath.Text = SettingsManager.SettingsCache.LastIcdPath;
            txtInputPath.Text = SettingsManager.SettingsCache.LastInputFilePath;

            SetImages();

        }

        private void SetImages()
        {
            var bitmap = IconChar.Ban.ToBitmap(24, Color.Red);
            btnClearForm.Image = bitmap;
            btnClearForm.ImageAlign = ContentAlignment.MiddleRight;

            var folderImage = IconChar.FolderOpen.ToBitmap(24, Color.Goldenrod);
            btnCPTFileDialog.Image = folderImage;
            btnCPTFileDialog.ImageAlign = ContentAlignment.MiddleLeft;
            btnCPTFileDialog.TextAlign = ContentAlignment.MiddleRight;

            btnOpenICDFileDialog.Image = folderImage;
            btnOpenICDFileDialog.ImageAlign = ContentAlignment.MiddleLeft;
            btnOpenICDFileDialog.TextAlign = ContentAlignment.MiddleRight;

            btnOpenInputFileDialog.Image = folderImage;
            btnOpenInputFileDialog.ImageAlign = ContentAlignment.MiddleLeft;
            btnOpenInputFileDialog.TextAlign = ContentAlignment.MiddleRight;

            var playImage = IconChar.Play.ToBitmap(24, Color.Green);
            btnProcess.Image = playImage;
            btnProcess.ImageAlign = ContentAlignment.MiddleRight;


            var exitImage = IconChar.SignOutAlt.ToBitmap(24, Color.Red);
            btnClose.Image = exitImage;
            btnClose.ImageAlign = ContentAlignment.MiddleRight;

            var infoIcon = IconChar.InfoCircle.ToBitmap(48, Color.Gray);
            pbStatus.Image = infoIcon;
        }

        private void LinkOpenInputFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(txtInputPath.Text);
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (FrmSettings frmSettings = new FrmSettings())
            {
                frmSettings.ShowDialog();
            }
        }

        private void BtnClearForm_Click(object sender, EventArgs e)
        {
            txtCptPath.Text = "";
            txtICDPath.Text = "";
            txtInputPath.Text = "";
            ClearValidationErrors();
        }

        private void BtnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }



        private void Button1_Click(object sender, EventArgs e)
        {
            if (File.Exists(txtCptPath.Text))
            {
                Process.Start(txtCptPath.Text);
            }

        }

        private void BtnViewIcdFile_Click(object sender, EventArgs e)
        {
            if (File.Exists(txtICDPath.Text))
            {
                Process.Start(txtICDPath.Text);
            }
        }
    }
}
