﻿namespace CECBOT
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCptPath = new System.Windows.Forms.TextBox();
            this.txtICDPath = new System.Windows.Forms.TextBox();
            this.txtInputPath = new System.Windows.Forms.TextBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.CptFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnOpenICDFileDialog = new System.Windows.Forms.Button();
            this.btnOpenInputFileDialog = new System.Windows.Forms.Button();
            this.IcdFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.InpuFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblInputError = new System.Windows.Forms.Label();
            this.grpSuccess = new System.Windows.Forms.GroupBox();
            this.pbStatus = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.linkOpenInputFile = new System.Windows.Forms.LinkLabel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.chkRemember = new System.Windows.Forms.CheckBox();
            this.btnClearForm = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCPTFileDialog = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnViewIcdFile = new System.Windows.Forms.Button();
            this.btnViewCptFile = new System.Windows.Forms.Button();
            this.lblICDError = new System.Windows.Forms.Label();
            this.lblCptError = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblMasterDataPathError = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.grpSuccess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(113, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "CPT Master List Path* :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(116, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "ICD Master List Path* :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(148, 316);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Input File Path* :";
            // 
            // txtCptPath
            // 
            this.txtCptPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCptPath.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCptPath.Location = new System.Drawing.Point(286, 135);
            this.txtCptPath.Name = "txtCptPath";
            this.txtCptPath.Size = new System.Drawing.Size(443, 26);
            this.txtCptPath.TabIndex = 3;
            // 
            // txtICDPath
            // 
            this.txtICDPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtICDPath.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtICDPath.Location = new System.Drawing.Point(286, 200);
            this.txtICDPath.Name = "txtICDPath";
            this.txtICDPath.Size = new System.Drawing.Size(443, 26);
            this.txtICDPath.TabIndex = 4;
            // 
            // txtInputPath
            // 
            this.txtInputPath.BackColor = System.Drawing.Color.White;
            this.txtInputPath.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputPath.Location = new System.Drawing.Point(286, 313);
            this.txtInputPath.Name = "txtInputPath";
            this.txtInputPath.Size = new System.Drawing.Size(443, 26);
            this.txtInputPath.TabIndex = 5;
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Location = new System.Drawing.Point(584, 399);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnProcess.Size = new System.Drawing.Size(148, 36);
            this.btnProcess.TabIndex = 6;
            this.btnProcess.Text = "Validate File";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // btnOpenICDFileDialog
            // 
            this.btnOpenICDFileDialog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnOpenICDFileDialog.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenICDFileDialog.Location = new System.Drawing.Point(748, 200);
            this.btnOpenICDFileDialog.Name = "btnOpenICDFileDialog";
            this.btnOpenICDFileDialog.Size = new System.Drawing.Size(100, 26);
            this.btnOpenICDFileDialog.TabIndex = 8;
            this.btnOpenICDFileDialog.Text = "Browse";
            this.btnOpenICDFileDialog.UseVisualStyleBackColor = false;
            this.btnOpenICDFileDialog.Click += new System.EventHandler(this.BtnOpenICDFileDialog_Click);
            // 
            // btnOpenInputFileDialog
            // 
            this.btnOpenInputFileDialog.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenInputFileDialog.Location = new System.Drawing.Point(748, 307);
            this.btnOpenInputFileDialog.Name = "btnOpenInputFileDialog";
            this.btnOpenInputFileDialog.Size = new System.Drawing.Size(100, 27);
            this.btnOpenInputFileDialog.TabIndex = 9;
            this.btnOpenInputFileDialog.Text = "Browse";
            this.btnOpenInputFileDialog.UseVisualStyleBackColor = true;
            this.btnOpenInputFileDialog.Click += new System.EventHandler(this.BtnOpenInputFileDialog_Click);
            // 
            // InpuFileDialog
            // 
            this.InpuFileDialog.FileName = "openFileDialog2";
            // 
            // lblInputError
            // 
            this.lblInputError.AutoSize = true;
            this.lblInputError.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputError.ForeColor = System.Drawing.Color.Red;
            this.lblInputError.Location = new System.Drawing.Point(262, 337);
            this.lblInputError.Name = "lblInputError";
            this.lblInputError.Size = new System.Drawing.Size(0, 15);
            this.lblInputError.TabIndex = 18;
            // 
            // grpSuccess
            // 
            this.grpSuccess.BackColor = System.Drawing.SystemColors.Control;
            this.grpSuccess.Controls.Add(this.pbStatus);
            this.grpSuccess.Controls.Add(this.label5);
            this.grpSuccess.Controls.Add(this.linkOpenInputFile);
            this.grpSuccess.Controls.Add(this.lblStatus);
            this.grpSuccess.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSuccess.ForeColor = System.Drawing.Color.Black;
            this.grpSuccess.Location = new System.Drawing.Point(135, 467);
            this.grpSuccess.Name = "grpSuccess";
            this.grpSuccess.Size = new System.Drawing.Size(709, 98);
            this.grpSuccess.TabIndex = 20;
            this.grpSuccess.TabStop = false;
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(11, 36);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(44, 37);
            this.pbStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbStatus.TabIndex = 29;
            this.pbStatus.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(500, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 23);
            this.label5.TabIndex = 21;
            this.label5.Text = "to open the file.";
            // 
            // linkOpenInputFile
            // 
            this.linkOpenInputFile.AutoSize = true;
            this.linkOpenInputFile.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkOpenInputFile.Location = new System.Drawing.Point(447, 44);
            this.linkOpenInputFile.Name = "linkOpenInputFile";
            this.linkOpenInputFile.Size = new System.Drawing.Size(51, 23);
            this.linkOpenInputFile.TabIndex = 20;
            this.linkOpenInputFile.TabStop = true;
            this.linkOpenInputFile.Text = "HERE";
            this.linkOpenInputFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkOpenInputFile_LinkClicked);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.SystemColors.Control;
            this.lblStatus.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Location = new System.Drawing.Point(60, 44);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(390, 23);
            this.lblStatus.TabIndex = 19;
            this.lblStatus.Text = "File has been validated successfully. Please click";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Cambria", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Navy;
            this.lblTitle.Location = new System.Drawing.Point(409, 37);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(271, 32);
            this.lblTitle.TabIndex = 23;
            this.lblTitle.Text = "CEC FILE VALIDATOR";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(833, 9);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(97, 22);
            this.linkLabel1.TabIndex = 24;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "SETTINGS";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
            // 
            // chkRemember
            // 
            this.chkRemember.AutoSize = true;
            this.chkRemember.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemember.Location = new System.Drawing.Point(286, 345);
            this.chkRemember.Name = "chkRemember";
            this.chkRemember.Size = new System.Drawing.Size(164, 22);
            this.chkRemember.TabIndex = 25;
            this.chkRemember.Text = "Remember last input?";
            this.chkRemember.UseVisualStyleBackColor = true;
            // 
            // btnClearForm
            // 
            this.btnClearForm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearForm.Location = new System.Drawing.Point(430, 399);
            this.btnClearForm.Name = "btnClearForm";
            this.btnClearForm.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnClearForm.Size = new System.Drawing.Size(145, 36);
            this.btnClearForm.TabIndex = 26;
            this.btnClearForm.Text = "Clear Form";
            this.btnClearForm.UseVisualStyleBackColor = true;
            this.btnClearForm.Click += new System.EventHandler(this.BtnClearForm_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(295, 399);
            this.btnClose.Name = "btnClose";
            this.btnClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnClose.Size = new System.Drawing.Size(119, 36);
            this.btnClose.TabIndex = 27;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click_1);
            // 
            // btnCPTFileDialog
            // 
            this.btnCPTFileDialog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(6)))), ((int)(((byte)(90)))), ((int)(((byte)(177)))));
            this.btnCPTFileDialog.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCPTFileDialog.Location = new System.Drawing.Point(748, 135);
            this.btnCPTFileDialog.Name = "btnCPTFileDialog";
            this.btnCPTFileDialog.Size = new System.Drawing.Size(100, 26);
            this.btnCPTFileDialog.TabIndex = 28;
            this.btnCPTFileDialog.Text = "Browse";
            this.btnCPTFileDialog.UseVisualStyleBackColor = false;
            this.btnCPTFileDialog.Click += new System.EventHandler(this.BtnCPTFileDialog_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.btnViewIcdFile);
            this.groupBox1.Controls.Add(this.btnViewCptFile);
            this.groupBox1.Controls.Add(this.lblICDError);
            this.groupBox1.Controls.Add(this.lblCptError);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lblMasterDataPathError);
            this.groupBox1.Location = new System.Drawing.Point(102, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(875, 172);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // btnViewIcdFile
            // 
            this.btnViewIcdFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnViewIcdFile.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewIcdFile.Location = new System.Drawing.Point(752, 92);
            this.btnViewIcdFile.Name = "btnViewIcdFile";
            this.btnViewIcdFile.Size = new System.Drawing.Size(100, 26);
            this.btnViewIcdFile.TabIndex = 33;
            this.btnViewIcdFile.Text = "View";
            this.btnViewIcdFile.UseVisualStyleBackColor = false;
            this.btnViewIcdFile.Click += new System.EventHandler(this.BtnViewIcdFile_Click);
            // 
            // btnViewCptFile
            // 
            this.btnViewCptFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnViewCptFile.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewCptFile.Location = new System.Drawing.Point(752, 27);
            this.btnViewCptFile.Name = "btnViewCptFile";
            this.btnViewCptFile.Size = new System.Drawing.Size(100, 26);
            this.btnViewCptFile.TabIndex = 30;
            this.btnViewCptFile.Text = "View";
            this.btnViewCptFile.UseVisualStyleBackColor = false;
            this.btnViewCptFile.Click += new System.EventHandler(this.Button1_Click);
            // 
            // lblICDError
            // 
            this.lblICDError.AutoSize = true;
            this.lblICDError.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblICDError.ForeColor = System.Drawing.Color.Red;
            this.lblICDError.Location = new System.Drawing.Point(185, 119);
            this.lblICDError.Name = "lblICDError";
            this.lblICDError.Size = new System.Drawing.Size(0, 15);
            this.lblICDError.TabIndex = 32;
            // 
            // lblCptError
            // 
            this.lblCptError.AutoSize = true;
            this.lblCptError.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCptError.ForeColor = System.Drawing.Color.Red;
            this.lblCptError.Location = new System.Drawing.Point(185, 56);
            this.lblCptError.Name = "lblCptError";
            this.lblCptError.Size = new System.Drawing.Size(0, 15);
            this.lblCptError.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(414, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "(OR)";
            // 
            // lblMasterDataPathError
            // 
            this.lblMasterDataPathError.AutoSize = true;
            this.lblMasterDataPathError.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasterDataPathError.ForeColor = System.Drawing.Color.Red;
            this.lblMasterDataPathError.Location = new System.Drawing.Point(185, 149);
            this.lblMasterDataPathError.Name = "lblMasterDataPathError";
            this.lblMasterDataPathError.Size = new System.Drawing.Size(0, 15);
            this.lblMasterDataPathError.TabIndex = 30;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(271, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1017, 596);
            this.Controls.Add(this.btnCPTFileDialog);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnClearForm);
            this.Controls.Add(this.chkRemember);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.grpSuccess);
            this.Controls.Add(this.lblInputError);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnOpenInputFileDialog);
            this.Controls.Add(this.btnOpenICDFileDialog);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.txtInputPath);
            this.Controls.Add(this.txtICDPath);
            this.Controls.Add(this.txtCptPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmMain";
            this.Text = "CEC BOT";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpSuccess.ResumeLayout(false);
            this.grpSuccess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCptPath;
        private System.Windows.Forms.TextBox txtICDPath;
        private System.Windows.Forms.TextBox txtInputPath;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.OpenFileDialog CptFileDialog;
        private System.Windows.Forms.Button btnOpenICDFileDialog;
        private System.Windows.Forms.Button btnOpenInputFileDialog;
        private System.Windows.Forms.OpenFileDialog IcdFileDialog;
        private System.Windows.Forms.OpenFileDialog InpuFileDialog;
        private System.Windows.Forms.Label lblInputError;
        private System.Windows.Forms.GroupBox grpSuccess;
        private System.Windows.Forms.LinkLabel linkOpenInputFile;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.CheckBox chkRemember;
        private System.Windows.Forms.Button btnClearForm;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCPTFileDialog;
        private System.Windows.Forms.PictureBox pbStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblICDError;
        private System.Windows.Forms.Label lblCptError;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblMasterDataPathError;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnViewIcdFile;
        private System.Windows.Forms.Button btnViewCptFile;
    }
}

