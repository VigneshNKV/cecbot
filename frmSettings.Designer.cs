﻿namespace CECBOT
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBrowseCpt = new System.Windows.Forms.Button();
            this.txtCptPath = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.cboCptFields = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnBrowseIcd = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtICDPath = new System.Windows.Forms.TextBox();
            this.cboIcdFields = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboNDCField = new System.Windows.Forms.ComboBox();
            this.tabInputFileSettings = new System.Windows.Forms.TabControl();
            this.tabPageInputFile = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.cboInputFileNcdFields = new System.Windows.Forms.ComboBox();
            this.cboInputFileBillingCommentsFields = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtInputPath = new System.Windows.Forms.TextBox();
            this.cboInputFields_Status = new System.Windows.Forms.ComboBox();
            this.btnColorPicker = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cboInputFields_ICD = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboInputFields_MRN = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHighlightColor = new System.Windows.Forms.TextBox();
            this.btnBrowseInputFile = new System.Windows.Forms.Button();
            this.cboInputFields_CPT = new System.Windows.Forms.ComboBox();
            this.tabPageCpt = new System.Windows.Forms.TabPage();
            this.txtCptRanges = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cboDescriptionFields = new System.Windows.Forms.ComboBox();
            this.tabPageIcd = new System.Windows.Forms.TabPage();
            this.tabInputFileSettings.SuspendLayout();
            this.tabPageInputFile.SuspendLayout();
            this.tabPageCpt.SuspendLayout();
            this.tabPageIcd.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveSettings.Location = new System.Drawing.Point(974, 447);
            this.btnSaveSettings.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(78, 31);
            this.btnSaveSettings.TabIndex = 11;
            this.btnSaveSettings.Text = "Save";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.BtnSaveSettings_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(34, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "SETTINGS";
            // 
            // btnBrowseCpt
            // 
            this.btnBrowseCpt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseCpt.Location = new System.Drawing.Point(105, 42);
            this.btnBrowseCpt.Name = "btnBrowseCpt";
            this.btnBrowseCpt.Size = new System.Drawing.Size(190, 27);
            this.btnBrowseCpt.TabIndex = 13;
            this.btnBrowseCpt.Text = "Browse Sample CPT File";
            this.btnBrowseCpt.UseVisualStyleBackColor = true;
            this.btnBrowseCpt.Click += new System.EventHandler(this.BtnBrowseCpt_Click);
            // 
            // txtCptPath
            // 
            this.txtCptPath.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCptPath.Location = new System.Drawing.Point(959, 6);
            this.txtCptPath.Name = "txtCptPath";
            this.txtCptPath.ReadOnly = true;
            this.txtCptPath.Size = new System.Drawing.Size(26, 26);
            this.txtCptPath.TabIndex = 14;
            this.txtCptPath.Visible = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // cboCptFields
            // 
            this.cboCptFields.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCptFields.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCptFields.FormattingEnabled = true;
            this.cboCptFields.Location = new System.Drawing.Point(104, 85);
            this.cboCptFields.Name = "cboCptFields";
            this.cboCptFields.Size = new System.Drawing.Size(191, 26);
            this.cboCptFields.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(47, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 18);
            this.label7.TabIndex = 16;
            this.label7.Text = "Code:*";
            // 
            // btnBrowseIcd
            // 
            this.btnBrowseIcd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseIcd.Location = new System.Drawing.Point(67, 27);
            this.btnBrowseIcd.Name = "btnBrowseIcd";
            this.btnBrowseIcd.Size = new System.Drawing.Size(191, 27);
            this.btnBrowseIcd.TabIndex = 13;
            this.btnBrowseIcd.Text = "Browse Sample ICD File";
            this.btnBrowseIcd.UseVisualStyleBackColor = true;
            this.btnBrowseIcd.Click += new System.EventHandler(this.BtnBrowseIcd_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 18);
            this.label8.TabIndex = 16;
            this.label8.Text = "Code:*";
            // 
            // txtICDPath
            // 
            this.txtICDPath.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtICDPath.Location = new System.Drawing.Point(908, 33);
            this.txtICDPath.Name = "txtICDPath";
            this.txtICDPath.ReadOnly = true;
            this.txtICDPath.Size = new System.Drawing.Size(33, 26);
            this.txtICDPath.TabIndex = 14;
            this.txtICDPath.Visible = false;
            // 
            // cboIcdFields
            // 
            this.cboIcdFields.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIcdFields.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboIcdFields.FormattingEnabled = true;
            this.cboIcdFields.Location = new System.Drawing.Point(67, 71);
            this.cboIcdFields.Name = "cboIcdFields";
            this.cboIcdFields.Size = new System.Drawing.Size(191, 26);
            this.cboIcdFields.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(52, 128);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 18);
            this.label13.TabIndex = 29;
            this.label13.Text = "NDC:*";
            // 
            // cboNDCField
            // 
            this.cboNDCField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNDCField.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNDCField.FormattingEnabled = true;
            this.cboNDCField.Location = new System.Drawing.Point(104, 120);
            this.cboNDCField.Name = "cboNDCField";
            this.cboNDCField.Size = new System.Drawing.Size(191, 26);
            this.cboNDCField.TabIndex = 28;
            // 
            // tabInputFileSettings
            // 
            this.tabInputFileSettings.Controls.Add(this.tabPageInputFile);
            this.tabInputFileSettings.Controls.Add(this.tabPageCpt);
            this.tabInputFileSettings.Controls.Add(this.tabPageIcd);
            this.tabInputFileSettings.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabInputFileSettings.Location = new System.Drawing.Point(35, 65);
            this.tabInputFileSettings.Name = "tabInputFileSettings";
            this.tabInputFileSettings.SelectedIndex = 0;
            this.tabInputFileSettings.Size = new System.Drawing.Size(1021, 375);
            this.tabInputFileSettings.TabIndex = 20;
            // 
            // tabPageInputFile
            // 
            this.tabPageInputFile.Controls.Add(this.label15);
            this.tabPageInputFile.Controls.Add(this.cboInputFileNcdFields);
            this.tabPageInputFile.Controls.Add(this.cboInputFileBillingCommentsFields);
            this.tabPageInputFile.Controls.Add(this.label14);
            this.tabPageInputFile.Controls.Add(this.txtInputPath);
            this.tabPageInputFile.Controls.Add(this.cboInputFields_Status);
            this.tabPageInputFile.Controls.Add(this.btnColorPicker);
            this.tabPageInputFile.Controls.Add(this.label5);
            this.tabPageInputFile.Controls.Add(this.cboInputFields_ICD);
            this.tabPageInputFile.Controls.Add(this.label3);
            this.tabPageInputFile.Controls.Add(this.label4);
            this.tabPageInputFile.Controls.Add(this.cboInputFields_MRN);
            this.tabPageInputFile.Controls.Add(this.label1);
            this.tabPageInputFile.Controls.Add(this.label2);
            this.tabPageInputFile.Controls.Add(this.txtHighlightColor);
            this.tabPageInputFile.Controls.Add(this.btnBrowseInputFile);
            this.tabPageInputFile.Controls.Add(this.cboInputFields_CPT);
            this.tabPageInputFile.Location = new System.Drawing.Point(4, 27);
            this.tabPageInputFile.Name = "tabPageInputFile";
            this.tabPageInputFile.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInputFile.Size = new System.Drawing.Size(1013, 344);
            this.tabPageInputFile.TabIndex = 0;
            this.tabPageInputFile.Text = "INPUT FILE";
            this.tabPageInputFile.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(13, 147);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 18);
            this.label15.TabIndex = 28;
            this.label15.Text = "NDC Column Name* :";
            // 
            // cboInputFileNcdFields
            // 
            this.cboInputFileNcdFields.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInputFileNcdFields.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputFileNcdFields.FormattingEnabled = true;
            this.cboInputFileNcdFields.Location = new System.Drawing.Point(178, 144);
            this.cboInputFileNcdFields.Name = "cboInputFileNcdFields";
            this.cboInputFileNcdFields.Size = new System.Drawing.Size(183, 26);
            this.cboInputFileNcdFields.TabIndex = 29;
            // 
            // cboInputFileBillingCommentsFields
            // 
            this.cboInputFileBillingCommentsFields.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInputFileBillingCommentsFields.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputFileBillingCommentsFields.FormattingEnabled = true;
            this.cboInputFileBillingCommentsFields.Location = new System.Drawing.Point(625, 139);
            this.cboInputFileBillingCommentsFields.Name = "cboInputFileBillingCommentsFields";
            this.cboInputFileBillingCommentsFields.Size = new System.Drawing.Size(184, 26);
            this.cboInputFileBillingCommentsFields.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(471, 142);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 18);
            this.label14.TabIndex = 26;
            this.label14.Text = "Billing Comments* :";
            // 
            // txtInputPath
            // 
            this.txtInputPath.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputPath.Location = new System.Drawing.Point(811, 12);
            this.txtInputPath.Margin = new System.Windows.Forms.Padding(4);
            this.txtInputPath.Name = "txtInputPath";
            this.txtInputPath.ReadOnly = true;
            this.txtInputPath.Size = new System.Drawing.Size(53, 26);
            this.txtInputPath.TabIndex = 20;
            this.txtInputPath.Visible = false;
            // 
            // cboInputFields_Status
            // 
            this.cboInputFields_Status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInputFields_Status.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputFields_Status.FormattingEnabled = true;
            this.cboInputFields_Status.Location = new System.Drawing.Point(625, 101);
            this.cboInputFields_Status.Name = "cboInputFields_Status";
            this.cboInputFields_Status.Size = new System.Drawing.Size(184, 26);
            this.cboInputFields_Status.TabIndex = 23;
            // 
            // btnColorPicker
            // 
            this.btnColorPicker.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColorPicker.Location = new System.Drawing.Point(370, 206);
            this.btnColorPicker.Margin = new System.Windows.Forms.Padding(4);
            this.btnColorPicker.Name = "btnColorPicker";
            this.btnColorPicker.Size = new System.Drawing.Size(78, 30);
            this.btnColorPicker.TabIndex = 10;
            this.btnColorPicker.Text = "Pick Color";
            this.btnColorPicker.UseVisualStyleBackColor = true;
            this.btnColorPicker.Click += new System.EventHandler(this.BtnColorPicker_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 206);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Highlight Color* :";
            // 
            // cboInputFields_ICD
            // 
            this.cboInputFields_ICD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInputFields_ICD.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputFields_ICD.FormattingEnabled = true;
            this.cboInputFields_ICD.Location = new System.Drawing.Point(625, 54);
            this.cboInputFields_ICD.Name = "cboInputFields_ICD";
            this.cboInputFields_ICD.Size = new System.Drawing.Size(184, 26);
            this.cboInputFields_ICD.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 99);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "MRN Column Name* :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(454, 104);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Status Column Name* :";
            // 
            // cboInputFields_MRN
            // 
            this.cboInputFields_MRN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInputFields_MRN.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputFields_MRN.FormattingEnabled = true;
            this.cboInputFields_MRN.Location = new System.Drawing.Point(178, 96);
            this.cboInputFields_MRN.Name = "cboInputFields_MRN";
            this.cboInputFields_MRN.Size = new System.Drawing.Size(183, 26);
            this.cboInputFields_MRN.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 54);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "CPT Column Name* :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(471, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "ICD Column Name* :";
            // 
            // txtHighlightColor
            // 
            this.txtHighlightColor.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighlightColor.Location = new System.Drawing.Point(179, 206);
            this.txtHighlightColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtHighlightColor.Name = "txtHighlightColor";
            this.txtHighlightColor.ReadOnly = true;
            this.txtHighlightColor.Size = new System.Drawing.Size(183, 26);
            this.txtHighlightColor.TabIndex = 9;
            // 
            // btnBrowseInputFile
            // 
            this.btnBrowseInputFile.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseInputFile.Location = new System.Drawing.Point(178, 11);
            this.btnBrowseInputFile.Name = "btnBrowseInputFile";
            this.btnBrowseInputFile.Size = new System.Drawing.Size(183, 27);
            this.btnBrowseInputFile.TabIndex = 17;
            this.btnBrowseInputFile.Text = "Browse  Sample Input File";
            this.btnBrowseInputFile.UseVisualStyleBackColor = true;
            this.btnBrowseInputFile.Click += new System.EventHandler(this.BtnBrowseInputFile_Click);
            // 
            // cboInputFields_CPT
            // 
            this.cboInputFields_CPT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInputFields_CPT.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputFields_CPT.FormattingEnabled = true;
            this.cboInputFields_CPT.Location = new System.Drawing.Point(178, 54);
            this.cboInputFields_CPT.Name = "cboInputFields_CPT";
            this.cboInputFields_CPT.Size = new System.Drawing.Size(183, 26);
            this.cboInputFields_CPT.TabIndex = 17;
            this.cboInputFields_CPT.SelectedIndexChanged += new System.EventHandler(this.CboInputFields_CPT_SelectedIndexChanged);
            // 
            // tabPageCpt
            // 
            this.tabPageCpt.Controls.Add(this.txtCptRanges);
            this.tabPageCpt.Controls.Add(this.label19);
            this.tabPageCpt.Controls.Add(this.label12);
            this.tabPageCpt.Controls.Add(this.cboDescriptionFields);
            this.tabPageCpt.Controls.Add(this.label13);
            this.tabPageCpt.Controls.Add(this.cboNDCField);
            this.tabPageCpt.Controls.Add(this.cboCptFields);
            this.tabPageCpt.Controls.Add(this.btnBrowseCpt);
            this.tabPageCpt.Controls.Add(this.txtCptPath);
            this.tabPageCpt.Controls.Add(this.label7);
            this.tabPageCpt.Location = new System.Drawing.Point(4, 27);
            this.tabPageCpt.Name = "tabPageCpt";
            this.tabPageCpt.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCpt.Size = new System.Drawing.Size(1013, 344);
            this.tabPageCpt.TabIndex = 1;
            this.tabPageCpt.Text = "CPT";
            this.tabPageCpt.UseVisualStyleBackColor = true;
            // 
            // txtCptRanges
            // 
            this.txtCptRanges.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCptRanges.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCptRanges.Location = new System.Drawing.Point(324, 63);
            this.txtCptRanges.Multiline = true;
            this.txtCptRanges.Name = "txtCptRanges";
            this.txtCptRanges.Size = new System.Drawing.Size(661, 126);
            this.txtCptRanges.TabIndex = 40;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(321, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(183, 18);
            this.label19.TabIndex = 39;
            this.label19.Text = "CPT Range Required for NDC";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 18);
            this.label12.TabIndex = 31;
            this.label12.Text = "Description:*";
            // 
            // cboDescriptionFields
            // 
            this.cboDescriptionFields.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDescriptionFields.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDescriptionFields.FormattingEnabled = true;
            this.cboDescriptionFields.Location = new System.Drawing.Point(104, 163);
            this.cboDescriptionFields.Name = "cboDescriptionFields";
            this.cboDescriptionFields.Size = new System.Drawing.Size(191, 26);
            this.cboDescriptionFields.TabIndex = 30;
            // 
            // tabPageIcd
            // 
            this.tabPageIcd.Controls.Add(this.btnBrowseIcd);
            this.tabPageIcd.Controls.Add(this.txtICDPath);
            this.tabPageIcd.Controls.Add(this.cboIcdFields);
            this.tabPageIcd.Controls.Add(this.label8);
            this.tabPageIcd.Location = new System.Drawing.Point(4, 27);
            this.tabPageIcd.Name = "tabPageIcd";
            this.tabPageIcd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageIcd.Size = new System.Drawing.Size(1013, 344);
            this.tabPageIcd.TabIndex = 2;
            this.tabPageIcd.Text = "ICD";
            this.tabPageIcd.UseVisualStyleBackColor = true;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 491);
            this.Controls.Add(this.tabInputFileSettings);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnSaveSettings);
            this.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmSettings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            this.tabInputFileSettings.ResumeLayout(false);
            this.tabPageInputFile.ResumeLayout(false);
            this.tabPageInputFile.PerformLayout();
            this.tabPageCpt.ResumeLayout(false);
            this.tabPageCpt.PerformLayout();
            this.tabPageIcd.ResumeLayout(false);
            this.tabPageIcd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnBrowseCpt;
        private System.Windows.Forms.TextBox txtCptPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ComboBox cboCptFields;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnBrowseIcd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtICDPath;
        private System.Windows.Forms.ComboBox cboIcdFields;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboNDCField;
        private System.Windows.Forms.TabControl tabInputFileSettings;
        private System.Windows.Forms.TabPage tabPageInputFile;
        private System.Windows.Forms.TextBox txtInputPath;
        private System.Windows.Forms.ComboBox cboInputFields_Status;
        private System.Windows.Forms.Button btnColorPicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboInputFields_ICD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboInputFields_MRN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHighlightColor;
        private System.Windows.Forms.Button btnBrowseInputFile;
        private System.Windows.Forms.ComboBox cboInputFields_CPT;
        private System.Windows.Forms.TabPage tabPageCpt;
        private System.Windows.Forms.TabPage tabPageIcd;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cboDescriptionFields;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboInputFileNcdFields;
        private System.Windows.Forms.ComboBox cboInputFileBillingCommentsFields;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCptRanges;
    }
}