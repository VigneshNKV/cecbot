﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CECBOT
{
    public static class SettingsManager
    {
        public static Settings SettingsCache { get; set; }
        public static async Task LoadSettings()
        {
            await Task.Run(() =>
           {
               var settingsJson = File.ReadAllText("appSettings.json");
               SettingsCache = JsonConvert.DeserializeObject<Settings>(settingsJson);
           });

        }

        public static async Task UpdateSettings()
        {
            await Task.Run(() =>
            {
                var settingsJson = JsonConvert.SerializeObject(SettingsCache, Formatting.Indented);
                File.WriteAllText("appSettings.json", settingsJson);
            });
        }
    }
}
