﻿using CECBOT.Extenders;
using FontAwesome.Sharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CECBOT
{
    public partial class FrmSettings : Form
    {
        private static readonly log4net.ILog _logger =log4net.LogManager.GetLogger(typeof(FrmSettings));
    
        public FrmSettings()
        {
            InitializeComponent();
        }

        private void FrmSettings_Load(object sender, EventArgs e)
        {
            var settings = SettingsManager.SettingsCache;
            if (!string.IsNullOrWhiteSpace(settings.CptFileSettings.SampleFileName))
            {
                var fileName = $"Resources/{Path.GetFileName(settings.CptFileSettings.SampleFileName)}";
                _logger.Info(fileName);
                CptCsvReader cptCsvReader = new CptCsvReader(fileName);
                txtCptPath.Text = fileName;
                var cptHeaders = cptCsvReader.GetHeaders();
                foreach (var item in cptHeaders)
                {
                    cboCptFields.Items.Add(item);
                    cboNDCField.Items.Add(item);
                    cboDescriptionFields.Items.Add(item);
                }

                cboCptFields.SelectedItem = settings.CptFileSettings.Code;
                cboNDCField.SelectedItem = settings.CptFileSettings.Ndc;
                cboDescriptionFields.SelectedItem = settings.CptFileSettings.Description;
            }

            if (!string.IsNullOrWhiteSpace(settings.IcdFileSettings.SampleFileName))
            {
                var fileName = $"Resources/{Path.GetFileName(settings.IcdFileSettings.SampleFileName)}";
                IcdCsvReader icdCsvReader = new IcdCsvReader(fileName);
                txtICDPath.Text = fileName;
                var icdHeaders = icdCsvReader.GetHeaders();
                foreach (var item in icdHeaders)
                {
                    cboIcdFields.Items.Add(item);
                }

                cboIcdFields.SelectedItem = settings.IcdFileSettings.Code;
            }

            if (!string.IsNullOrWhiteSpace(settings.InputFileSettings.SampleFileName))
            {
                var fileName = $"Resources/{Path.GetFileName(settings.InputFileSettings.SampleFileName)}";
                txtInputPath.Text = fileName;
              
                var inputFileHeaders = ExcelFileProcessor.GetHeaders(fileName);

                foreach (var item in inputFileHeaders)
                {
                    cboInputFields_CPT.Items.Add(item);
                    cboInputFields_ICD.Items.Add(item);
                    cboInputFields_MRN.Items.Add(item);
                    cboInputFields_Status.Items.Add(item);
                    cboInputFileBillingCommentsFields.Items.Add(item);
                    cboInputFileNcdFields.Items.Add(item);
                }

                cboInputFields_CPT.SelectedItem = settings.InputFileSettings.CptColumnName;
                cboInputFields_ICD.SelectedItem = settings.InputFileSettings.IcdColumnName;
                cboInputFields_MRN.SelectedItem = settings.InputFileSettings.MrnColumnName;
                cboInputFields_Status.SelectedItem = settings.InputFileSettings.StatusColumnName;
                cboInputFileBillingCommentsFields.SelectedItem = settings.InputFileSettings.BillingCommentsColumnName;
                cboInputFileNcdFields.SelectedItem = settings.InputFileSettings.NdcColumnName;

                StringBuilder sb = new StringBuilder();
                if (settings.CptFileSettings.Series != null && settings.CptFileSettings.Series.Any())
                {
                    foreach (var item in settings.CptFileSettings.Series)
                    {
                        sb.Append($"{item},");
                    }
                }

                if (settings.CptFileSettings.CPTRange != null && settings.CptFileSettings.CPTRange.Any())
                {
                    foreach (var item in settings.CptFileSettings.CPTRange)
                    {
                        sb.Append($"{item.Range},");
                    }
                }

                sb.Length--;

                txtCptRanges.Text = sb.ToString();

            }

            ColorConverter colorConverter = new ColorConverter();
            Color color = (Color)colorConverter.ConvertFromString(settings.InputFileSettings.HighlightColor);
            txtHighlightColor.BackColor = color;

        }

        private void BtnColorPicker_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                txtHighlightColor.BackColor = colorDialog.Color;
            }
        }

        private async void BtnSaveSettings_Click(object sender, EventArgs e)
        {
            var settings = SettingsManager.SettingsCache;

            settings.InputFileSettings.CptColumnName = cboInputFields_CPT.Text;
            settings.InputFileSettings.IcdColumnName = cboInputFields_ICD.Text;
            settings.InputFileSettings.MrnColumnName = cboInputFields_MRN.Text;
            settings.InputFileSettings.StatusColumnName = cboInputFields_Status.Text;
            settings.InputFileSettings.BillingCommentsColumnName = cboInputFileBillingCommentsFields.Text;
            settings.InputFileSettings.NdcColumnName = cboInputFileNcdFields.Text;

            settings.IcdFileSettings.Code = cboIcdFields.Text;
            settings.CptFileSettings.Code = cboCptFields.Text;
            settings.CptFileSettings.Ndc = cboNDCField.Text;
            settings.CptFileSettings.Description = cboDescriptionFields.Text;
            settings.InputFileSettings.HighlightColor = $"#{(txtHighlightColor.BackColor.ToArgb() & 0x00FFFFFF).ToString("X6")}";

            settings.InputFileSettings.SampleFileName = Path.GetFileName(txtInputPath.Text);
            settings.CptFileSettings.SampleFileName = Path.GetFileName(txtCptPath.Text);
            settings.IcdFileSettings.SampleFileName = Path.GetFileName(txtICDPath.Text);

            PopulateCPTRanges(settings);

            await SettingsManager.UpdateSettings();

            MessageBox.Show("Settings saved successfully", "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void PopulateCPTRanges(Settings settings)
        {
            var rangeArray = txtCptRanges.Text.Split(',');
            settings.CptFileSettings.CPTRange = new List<CPTRange>();
            settings.CptFileSettings.Series = new List<string>();

            foreach (var item in rangeArray)
            {
                var cleansedValue = item.Replace(Environment.NewLine, string.Empty).Trim();
                if (cleansedValue.Contains("-"))
                {
                    settings.CptFileSettings.CPTRange.Add(new CPTRange(cleansedValue));
                }
                else
                {
                    settings.CptFileSettings.Series.Add(cleansedValue);
                }
            }
        }

        private void BtnBrowseCpt_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "CSV Files|*.csv;";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtCptPath.Text = openFileDialog.FileName;
                File.Copy(openFileDialog.FileName, $"Resources/{Path.GetFileName("CPT_TEMPLATE.csv")}", overwrite: true);
                CptCsvReader cptCsvReader = new CptCsvReader(openFileDialog.FileName);
                var headers = cptCsvReader.GetHeaders();

                cboCptFields.Items.Clear();
                foreach (var item in headers)
                {
                    var cleansedValue = item.RemoveDoubleQuotes();
                    cboCptFields.Items.Add(cleansedValue);
                    cboNDCField.Items.Add(cleansedValue);
                    cboDescriptionFields.Items.Add(cleansedValue);
                }
            }
        }

        private void BtnBrowseIcd_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "CSV Files|*.csv;";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtICDPath.Text = openFileDialog.FileName;
                File.Copy(openFileDialog.FileName, $"Resources/{Path.GetFileName("ICD_TEMPLATE.csv")}", overwrite: true);

                IcdCsvReader csvReader = new IcdCsvReader(openFileDialog.FileName);
                var headers = csvReader.GetHeaders();

                cboIcdFields.Items.Clear();

                foreach (var item in headers)
                {
                    cboIcdFields.Items.Add(item.Replace("\"", string.Empty));
                }
            }
        }

        private void BtnBrowseInputFile_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Excel Files|*.xlsx;";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {

                txtInputPath.Text = openFileDialog.FileName;


                File.Copy(openFileDialog.FileName, $"Resources/{Path.GetFileName("INPUT_TEMPLATE.xlsx")}", overwrite: true);

                var headers = ExcelFileProcessor.GetHeaders(openFileDialog.FileName);

                cboInputFields_CPT.Items.Clear();
                cboInputFields_ICD.Items.Clear();
                cboInputFields_MRN.Items.Clear();
                cboInputFields_Status.Items.Clear();
                cboInputFileBillingCommentsFields.Items.Clear();
                cboInputFileNcdFields.Items.Clear();

                foreach (var item in headers)
                {
                    cboInputFields_CPT.Items.Add(item.Trim());
                    cboInputFields_ICD.Items.Add(item.Trim());
                    cboInputFields_MRN.Items.Add(item.Trim());
                    cboInputFields_Status.Items.Add(item.Trim());
                    cboInputFileBillingCommentsFields.Items.Add(item.Trim());
                    cboInputFileNcdFields.Items.Add(item.Trim());
                }
            }
        }

        private void CboInputFields_CPT_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        //private void btnAddCptRange_Click(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrWhiteSpace(txtCptStart.Text)
        //        && !string.IsNullOrWhiteSpace(txtCptEnd.Text)
        //        && txtCptStart.Text != _startText
        //        && txtCptEnd.Text != _endText)
        //    {
        //        if (!listCptRanges.Items.Contains($"{txtCptStart.Text}-{txtCptEnd.Text}"))
        //        {
        //            listCptRanges.Items.Add($"{txtCptStart.Text}-{txtCptEnd.Text}");
        //            txtCptStart.Text = _startText;
        //            txtCptEnd.Text = _endText;
        //        }
        //    }
        //}
    }
}
