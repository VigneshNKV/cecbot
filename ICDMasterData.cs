﻿namespace CECBOT
{
    public class ICDMasterData
    {
        public string Code { get; set; }

        public string Description { get; set; }

    }
}
