﻿namespace CECBOT
{
    public class Settings
    {
        public InputFileSettings InputFileSettings { get; set; }
        public CptFileSettings CptFileSettings { get; set; }
        public IcdFileSettings IcdFileSettings { get; set; }
        public string LastCptPath { get; set; }
        public string LastIcdPath { get; set; }
        public string LastInputFilePath { get; set; }

        public Settings()
        {
            InputFileSettings = new InputFileSettings() { HighlightColor = "#FFFF00" };
            CptFileSettings = new CptFileSettings();
            IcdFileSettings = new IcdFileSettings() { };
        }
    }
}
