﻿using System.Text.RegularExpressions;

namespace CECBOT.Extenders
{
    public static class StringExtenders
    {
        public static string RemoveDoubleQuotes(this string str)
        {
            if (!string.IsNullOrWhiteSpace(str))
            {
                return str.Replace("\"", string.Empty);
            }

            return str;
        }

        public static bool IsOnlyAlphabets(this string str)
        {

            if (!string.IsNullOrWhiteSpace(str))
            {
                return Regex.IsMatch(str, @"^[a-zA-Z]+$");
            }

            return false;
        }
    }
}
